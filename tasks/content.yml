---
- name: Create nginx dirs
  ansible.builtin.file:
    path: ".local/share/containers/storage/volumes/nginx-data/_data/{{ item }}"
    state: directory
    mode: '{{ nginx_dir_mode }}'
  loop:
    - images
    - kickstart
    - legacy
    - uefi
    - f{{ fedora_version }}
    - EFI/BOOT/x86_64-efi

- name: Deploy files for legacy bios boot
  ansible.builtin.copy:
    src: /usr/share/syslinux/{{ item }}
    dest: .local/share/containers/storage/volumes/nginx-data/_data/legacy/{{ item }}
    mode: '{{ nginx_file_mode }}'
    setype: container_file_t
  loop:
    - pxelinux.0
    - menu.c32
    - vesamenu.c32
    - ldlinux.c32
    - libcom32.c32
    - libutil.c32

- name: Create pxelinux.cfg dir
  ansible.builtin.file:
    path: ".local/share/containers/storage/volumes/nginx-data/_data/legacy/pxelinux.cfg"
    state: directory
    mode: '{{ nginx_dir_mode }}'

- name: Deploy legacy boot cfg
  ansible.builtin.template:
    src: default
    dest: ".local/share/containers/storage/volumes/nginx-data/_data/legacy/pxelinux.cfg/default"
    mode: '{{ nginx_file_mode }}'
    setype: container_file_t

- name: Deploy grub.cfg
  ansible.builtin.template:
    src: grub.cfg
    dest: ".local/share/containers/storage/volumes/nginx-data/_data/uefi/grub.cfg"
    mode: '{{ nginx_file_mode }}'
    setype: container_file_t

- name: Create pxelinux.cfg dir
  ansible.builtin.file:
    path: ".local/share/containers/storage/volumes/nginx-data/_data/uefi/f{{ fedora_version }}"
    state: directory
    mode: '{{ nginx_dir_mode }}'

# Needed by Legacy Client: GET /f41/vmlinuz HTTP/1.1 ... as specified in grub.cfg
- name: Populate f{{ fedora_version }}/
  ansible.builtin.get_url:
    url: "https://dl.fedoraproject.org/pub/fedora/linux/releases/{{ fedora_version }}/Everything/x86_64/os/images/pxeboot/{{ item }}"
    dest: ".local/share/containers/storage/volumes/nginx-data/_data/uefi/f{{ fedora_version }}/{{ item }}"
    mode: '{{ nginx_file_mode }}'
    setype: container_file_t
  loop:
    - initrd.img
    - vmlinuz

- name: Ensure mode of files in f{{ fedora_version }}/
  ansible.builtin.file:
    path: ".local/share/containers/storage/volumes/nginx-data/_data/uefi/f{{ fedora_version }}/{{ item }}"
    mode: '{{ nginx_file_mode }}'
    setype: container_file_t
  loop:
    - initrd.img
    - vmlinuz

- name: Deploy uefi/grubx64.efi
  ansible.builtin.get_url:
    url: https://dl.fedoraproject.org/pub/fedora/linux/releases/{{ fedora_version }}/Everything/x86_64/os/EFI/BOOT/grubx64.efi
    dest: .local/share/containers/storage/volumes/nginx-data/_data/uefi/grubx64.efi
    mode: '{{ nginx_file_mode }}'
    setype: container_file_t

- name: Download shim rpm
  ansible.builtin.get_url:
    url: https://dl.fedoraproject.org/pub/fedora/linux/releases/{{ fedora_version }}/Everything/x86_64/os/Packages/s/{{ shim_x64_version }}.rpm
    dest: /tmp/.
    mode: '0600'

- name: Extract shimx64.efi
  ansible.builtin.shell:
    cmd: rpm2cpio /tmp/{{ shim_x64_version }}.rpm | cpio -ivdm ./boot/efi/EFI/fedora/shimx64.efi
  args:
    chdir: /tmp
    creates: /tmp/boot/efi/EFI/fedora/shimx64.efi
  tags:
    - skip_ansible_lint

- name: Deploy uefi/shimx64.efi
  ansible.builtin.copy:
    remote_src: true
    src: /tmp/boot/efi/EFI/fedora/shimx64.efi
    dest: .local/share/containers/storage/volumes/nginx-data/_data/uefi/shimx64.efi
    mode: '{{ nginx_file_mode }}'
    setype: container_file_t

- name: Download {{ grub2_efi_x64_modules_version }}
  ansible.builtin.get_url:
    url: https://dl.fedoraproject.org/pub/fedora/linux/releases/{{ fedora_version }}/Everything/x86_64/os/Packages/g/{{ grub2_efi_x64_modules_version }}.rpm
    dest: /tmp/
    mode: '0600'

- name: Extract progress.mod
  ansible.builtin.shell:
    cmd: rpm2cpio /tmp/{{ grub2_efi_x64_modules_version }}.rpm | cpio -ivdm ./usr/lib/grub/x86_64-efi/progress.mod
  args:
    chdir: /tmp
    creates: /tmp/usr/lib/grub/x86_64-efi/progress.mod
  tags:
    - skip_ansible_lint

- name: Deploy progress.mod
  ansible.builtin.copy:
    remote_src: true
    src: /tmp/usr/lib/grub/x86_64-efi/progress.mod
    dest: .local/share/containers/storage/volumes/nginx-data/_data/EFI/BOOT/x86_64-efi/progress.mod
    mode: '{{ nginx_file_mode }}'
    setype: container_file_t

- name: Deploy anaconda-ks.cfg
  ansible.builtin.template:
    src: anaconda-ks.cfg
    dest: .local/share/containers/storage/volumes/nginx-data/_data/kickstart/anaconda-ks.cfg
    mode: '{{ nginx_file_mode }}'
    setype: container_file_t

- name: Check if install.img exists
  ansible.builtin.stat:
    path: ".local/share/containers/storage/volumes/nginx-data/_data/images/install.img"
  register: img_file

- name: Deploy install.img
  when: not img_file.stat.exists
  ansible.builtin.get_url:
    url: https://dl.fedoraproject.org/pub/fedora/linux/releases/{{ fedora_version }}/Everything/x86_64/os/images/install.img
    dest: .local/share/containers/storage/volumes/nginx-data/_data/images/install.img
    mode: '{{ nginx_file_mode }}'
    setype: container_file_t

- name: Ensure permissions install.img
  ansible.builtin.file:
    path: .local/share/containers/storage/volumes/nginx-data/_data/images/install.img
    mode: '{{ nginx_file_mode }}'
    setype: container_file_t
