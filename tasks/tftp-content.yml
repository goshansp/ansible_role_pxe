---
- name: Create tftp dirs
  ansible.builtin.file:
    path: '{{ tftp_data_path }}{{ item }}'
    state: directory
    mode: '{{ tftp_dir_mode }}'
    setype: container_file_t
  loop:
    - uefi
    - f{{ fedora_version }}
    - EFI/BOOT/x86_64-efi
    - EFI/fedora/x86_64-efi
    - pxelinux.cfg

- name: Deploy files for legacy bios boot
  ansible.builtin.copy:
    src: /usr/share/syslinux/{{ item }}
    dest: '{{ tftp_data_path }}{{ item }}'
    mode: '{{ tftp_file_mode }}'
    setype: container_file_t
  loop:
    - pxelinux.0
    - menu.c32
    - vesamenu.c32
    - ldlinux.c32
    - libcom32.c32
    - libutil.c32

- name: Deploy legacy boot cfg
  ansible.builtin.template:
    src: default
    dest: '{{ tftp_data_path }}pxelinux.cfg/default'
    mode: '{{ tftp_file_mode }}'
    setype: container_file_t

- name: Deploy /tftpboot/grub.cfg
  ansible.builtin.template:
    src: grub.cfg
    dest: '{{ tftp_data_path }}grub.cfg'
    mode: '{{ tftp_file_mode }}'
    setype: container_file_t

- name: Populate /tftpboot/f{{ fedora_version }}/
  ansible.builtin.get_url:
    url: "https://dl.fedoraproject.org/pub/fedora/linux/releases/{{ fedora_version }}/Everything/x86_64/os/images/pxeboot/{{ item }}"
    dest: "{{ tftp_data_path }}f{{ fedora_version }}/{{ item }}"
    mode: '{{ tftp_file_mode }}'
    setype: container_file_t
  loop:
    - initrd.img
    - vmlinuz

- name: Ensure mode of files in /tftpboot/f{{ fedora_version }}/
  ansible.builtin.file:
    path: '{{ tftp_data_path }}f{{ fedora_version }}/{{ item }}'
    mode: '{{ tftp_file_mode }}'
    setype: container_file_t
  loop:
    - initrd.img
    - vmlinuz

- name: Deploy /tftpboot/uefi/grubx64.efi
  ansible.builtin.get_url:
    url: https://dl.fedoraproject.org/pub/fedora/linux/releases/{{ fedora_version }}/Everything/x86_64/os/EFI/BOOT/grubx64.efi
    dest: '{{ tftp_data_path }}uefi/grubx64.efi'
    mode: '{{ tftp_file_mode }}'
    setype: container_file_t

- name: Ensure mode of /tftpboot/uefi/grubx64.efi
  ansible.builtin.file:
    path: '{{ tftp_data_path }}uefi/grubx64.efi'
    mode: '{{ tftp_file_mode }}'
    setype: container_file_t

- name: Download shim rpm
  ansible.builtin.get_url:
    url: https://dl.fedoraproject.org/pub/fedora/linux/releases/{{ fedora_version }}/Everything/x86_64/os/Packages/s/{{ shim_x64_version }}.rpm
    dest: /tmp/.
    mode: '0600'

- name: Extract shimx64.efi
  ansible.builtin.shell:
    cmd: rpm2cpio /tmp/{{ shim_x64_version }}.rpm | cpio -ivdm ./boot/efi/EFI/fedora/shimx64.efi
  args:
    chdir: /tmp
    creates: /tmp/boot/efi/EFI/fedora/shimx64.efi
  tags:
    - skip_ansible_lint

- name: Deploy /tftpboot/uefi/shimx64.efi
  ansible.builtin.copy:
    remote_src: true
    src: /tmp/boot/efi/EFI/fedora/shimx64.efi
    dest: "{{ tftp_data_path }}uefi/shimx64.efi"
    mode: '{{ tftp_file_mode }}'
    setype: container_file_t

- name: Download {{ grub2_efi_x64_modules_version }}
  ansible.builtin.get_url:
    url: https://dl.fedoraproject.org/pub/fedora/linux/releases/{{ fedora_version }}/Everything/x86_64/os/Packages/g/{{ grub2_efi_x64_modules_version }}.rpm
    dest: /tmp/
    mode: '0600'

- name: Extract progress.mod
  ansible.builtin.shell:
    cmd: rpm2cpio /tmp/{{ grub2_efi_x64_modules_version }}.rpm | cpio -ivdm ./usr/lib/grub/x86_64-efi/progress.mod
  args:
    chdir: /tmp
    creates: /tmp/usr/lib/grub/x86_64-efi/progress.mod
  tags:
    - skip_ansible_lint

- name: Deploy /tftpboot/EFI/fedora/x86_64-efi/progress.mod
  ansible.builtin.copy:
    remote_src: true
    src: /tmp/usr/lib/grub/x86_64-efi/progress.mod
    dest: '{{ tftp_data_path }}EFI/BOOT/x86_64-efi/progress.mod'
    mode: '{{ tftp_file_mode }}'
    setype: container_file_t
