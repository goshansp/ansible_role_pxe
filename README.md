# ansible_role_pxe

PXE server (UEFI and Legacy Boot) to deploy EFI ostree based OS. This role deploys podman containers enabeling a F12-stage Fedora Silverblue systems within the network. PXE booting is piggy-backing ontop of untouched/unowned dhcp services.


# Known Issues
- http uefi process is veeery slow as it cycles to other unstupportet tftp/pxe phases on vm bootloader ... should we revert to tftp?
- Kickstart reboot will halt T440s
- About every 10th boot on T440s will hang at Fetching Netboot Image. TFTP loggs get of grubx64.efi but no grub.cfg
- dnsmasq/tftp seems to switch user and cannot get root/0600 files while requiring 0644 ...
- some clients prefer tftp and go to http as fallback ...

# Prerequisites
```
ansible_role_minion or:

$ sysctl net.ipv4.ip_unprivileged_port_start=0

```

# TODO
- test ssh key login via ansible-pull.service
- update sequence diagram
- unify nginx-data / tftp-data directory into one?

- why are files not inheriting selinux label from parent?
- ssh key login not working ... remove password hash
- firewalling for  http 80 ...
- move http to https?


# Manual Test from Virtual Machine Manager (UI)
1. Create new VM
1. Manual Install
1. Fedora Silverblue 40
1. ...

1. Name = silverblue40-pxe-test
1. Check Flag Customize configuration before install
1. Bridge device ... virbr0
1. Finish

1. Option: UEFI: Change Firmware to UEFI -> Apply
1. Boot Options: Change boot order: NIC first
1. Begin Installation


# Legacy: Debug
```
$ systemctl --user restart nginx.service
$ systemctl --user restart dnsmasq.service
$ podman logs -f nginx
$ podman logs -f dnsmasq

$ podman exec -it dnsmasq /bin/sh

$ sudo journalctl -f
$ sudo systemctl stop firewalld.service
$ sudo setenforce 0

$ cat ~/.local/share/containers/storage/volumes/tftpboot-storage/_data/grub.cfg
# the following lines should be part of ansible_role_ostree
# needed to change disk name (sda/nvme)
$ vi /var/srv/www/kickstart/anaconda-ks.cfg
```


# Flow / Sequence Diagram (EFI)
```mermaid
sequenceDiagram
    Client->>3rd Party DHCP Server: DHCP Discover (Broadcast)
    Client->>PXE Server: DHCP Discover (Broadcast)
    3rd Party DHCP Server->>EFI Client: DHCP Offer (Which we do not control)
    PXE Server->>Client: PXE Offer (nextserver, bootfile)
    Client->>PXE Server: uefi/shimx64.efi
    Client->>PXE Server: grubx64.efi grub.cfg vmlinuz initrd.img
    Client->>PXE Server: get anaconda-ks.cfg install.img
    Client->>Quay.io: get goshansp Silverblue Spin
```


# TBD: On Automated Testing
Should we create kvm (legacy/uefi) to test the stack? How would we validate the proper stating of the box?


# Setup of Development Environment
The fcos qcow2 as indicated in `molecule/default/molecule.yml needs to be hosted on ansible_role_ostree's nginx.
```
$ rpm-ostree install gcc guestfs-tools libvirt libvirt-client libvirt-devel python3-devel qemu-kvm-core -y
$ git clone git@gitlab.com:goshansp/ansible_role_pxe
$ python -m venv venv-pxe; source venv-pxe/bin/activate; cd ansible_role_pxe; pip install wheel; pip install --upgrade pip
$ pip install -r requirements.txt
$ molecule test
```

# ADR

## 1: DNSMASQ TFTP -> HPA
Ripped out tftp because dnsmasq is suspected to drop priv (change user) to serve files. Seems to have no fix as container besides loosening permissions. Http is faster than tftp and less complex. Works with VM (Legacy Bios), looking forward to test it on Supermicro. Legacy VM is using iPXE which is advanced and can to HTTP. Uefi VM is falling back to HTTP eventually but slow and no way to speed up. We need TFTP for the legacy / flexibility and hence move to TFTPD-HPA.


### Uefi VM Clients cannot get the http shimx64.efi ...
```
Start PXE over IPv4
Station IP address is 192.168.122.218

Server IP address is 192.168.122.229
NBP filename is http://192.168.122.229/uefi/shimx64.efi
NBP filesize is 0 Bytes
PXE-E99: Unexpected Network Error
BdsDxe: failed to load Boot0001 ...

tcpdump
fcos-41.hp.molecule.lab > 192.168.122.218: ICMP fcos-41.hp.molecule.lab udp port tftp unreachable, length 118

Start http boot over ipv4
Dec 27 20:41:24 fcos-41.hp.molecule.lab dnsmasq[16426]: dnsmasq-dhcp: 786923243 vendor class: HTTPClient:Arch:00016:UNDI:003001
```